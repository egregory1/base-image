FROM redhat/ubi8:latest

USER root

RUN yum install https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm -y
RUN yum install -y yum-utils
RUN yum-config-manager --add-repo https://download.docker.com/linux/rhel/docker-ce.repo
RUN yum repolist
RUN /usr/bin/crb enable
RUN yum -y install procps-ng
RUN yum install python3 -y
RUN alternatives --set python /usr/bin/python3
RUN python -m pip install --upgrade pip
RUN pip3 install ansible 
#COPY rhel8STIG-ansible .
#RUN sed $'s/\r$//' ./enforce.sh > ./enforce.Unix.sh
#RUN chmod u+x enforce.Unix.sh
#RUN ./enforce.Unix.sh
#Run ansible-playbook -v -b -i /dev/null --check site.yml

